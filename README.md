Just a quick example demostrating two methods for saving and restoring a trained tensorflow model. 

- First run save_model.ipynb to create a simple model and save it to /tmp
- Then see load_model.ipynb to load that simple model values 
- Third (optional) is to run load_graph_get_tensors.ipynb to load the model graph and get the tensors. This provides an alternative to using the model without redefining the graph.